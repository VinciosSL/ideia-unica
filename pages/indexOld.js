import {useState} from 'react'

const Home2 = () => {
    return (
        <div>
            <h2>Pagina Estatica - tradicional React - gerenerate</h2>
           <h2>Home.</h2>
        <a href="/sobre">Sobre</a>

            <Contador/>
        </div>
    )
}

function Contador(){

    const [contador, setContador]=useState(0)

    function adicionarContador(p){
        setContador(contador+1)

    }

    return(

        <div>
            <div><h3>{contador}</h3></div>
            <button onClick={adicionarContador}>Adicionar</button>
        </div>
    )
}


export default Home2