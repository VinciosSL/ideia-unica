"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/tempo";
exports.ids = ["pages/tempo"];
exports.modules = {

/***/ "./pages/tempo.js":
/*!************************!*\
  !*** ./pages/tempo.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\nvar _jsxFileName = \"/home/vini/git/learning/next/felipeDechamps/ideia-unica/pages/tempo.js\";\n\n\nfunction Tempo() {\n  const dynamicDate = new Date();\n  const dynamicDateString = dynamicDate.toGMTString();\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n    children: [dynamicDateString, \" (Din\\xE2mico) - conforme atualiza\\xE7\\xE3o da pagina\"]\n  }, void 0, true, {\n    fileName: _jsxFileName,\n    lineNumber: 7,\n    columnNumber: 9\n  }, this);\n}\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Tempo);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy90ZW1wby5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSxTQUFTQSxLQUFULEdBQWdCO0FBQ1osUUFBTUMsV0FBVyxHQUFHLElBQUlDLElBQUosRUFBcEI7QUFDQSxRQUFNQyxpQkFBaUIsR0FBR0YsV0FBVyxDQUFDRyxXQUFaLEVBQTFCO0FBR0Esc0JBQ0k7QUFBQSxlQUNLRCxpQkFETDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQU9IOztBQUVELGlFQUFlSCxLQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vaWRlaWEtdW5pY2EvLi9wYWdlcy90ZW1wby5qcz84MmIwIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIFRlbXBvKCl7XG4gICAgY29uc3QgZHluYW1pY0RhdGUgPSBuZXcgRGF0ZSgpXG4gICAgY29uc3QgZHluYW1pY0RhdGVTdHJpbmcgPSBkeW5hbWljRGF0ZS50b0dNVFN0cmluZygpXG5cblxuICAgIHJldHVybihcbiAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIHtkeW5hbWljRGF0ZVN0cmluZ30gKERpbsOibWljbykgLSBjb25mb3JtZSBhdHVhbGl6YcOnw6NvIGRhIHBhZ2luYVxuICAgICAgICA8L2Rpdj5cbiAgICApXG5cblxufVxuXG5leHBvcnQgZGVmYXVsdCBUZW1wbyJdLCJuYW1lcyI6WyJUZW1wbyIsImR5bmFtaWNEYXRlIiwiRGF0ZSIsImR5bmFtaWNEYXRlU3RyaW5nIiwidG9HTVRTdHJpbmciXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/tempo.js\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/tempo.js"));
module.exports = __webpack_exports__;

})();