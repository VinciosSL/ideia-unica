"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/tempo2";
exports.ids = ["pages/tempo2"];
exports.modules = {

/***/ "./pages/tempo2.js":
/*!*************************!*\
  !*** ./pages/tempo2.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"getStaticProps\": () => (/* binding */ getStaticProps),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\nvar _jsxFileName = \"/home/vini/git/learning/next/felipeDechamps/ideia-unica/pages/tempo2.js\";\n\n\n\nfunction Tempo(props) {\n  const dynamicDate = new Date();\n  const dynamicDateString = dynamicDate.toGMTString();\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n      children: [dynamicDateString, \" (Din\\xE2mico) - \"]\n    }, void 0, true, {\n      fileName: _jsxFileName,\n      lineNumber: 7,\n      columnNumber: 13\n    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n      children: [props.staticDateString, \"  (Est\\xE1tico)getStaticProps - roda uma vez no build de produ\\xE7\\xE3o\"]\n    }, void 0, true, {\n      fileName: _jsxFileName,\n      lineNumber: 8,\n      columnNumber: 13\n    }, this)]\n  }, void 0, true);\n}\n\nfunction getStaticProps() {\n  const staticDate = new Date();\n  const staticDateString = staticDate.toGMTString();\n  return {\n    props: {\n      staticDateString\n    }\n  };\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Tempo);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy90ZW1wbzIuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQSxTQUFTQSxLQUFULENBQWVDLEtBQWYsRUFBcUI7QUFDakIsUUFBTUMsV0FBVyxHQUFHLElBQUlDLElBQUosRUFBcEI7QUFDQSxRQUFNQyxpQkFBaUIsR0FBR0YsV0FBVyxDQUFDRyxXQUFaLEVBQTFCO0FBQ0Esc0JBQ0o7QUFBQSw0QkFFWTtBQUFBLGlCQUFNRCxpQkFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGWixlQUdZO0FBQUEsaUJBQU1ILEtBQUssQ0FBQ0ssZ0JBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSFo7QUFBQSxrQkFESTtBQVFDOztBQUNFLFNBQVNDLGNBQVQsR0FBeUI7QUFDNUIsUUFBTUMsVUFBVSxHQUFHLElBQUlMLElBQUosRUFBbkI7QUFDQSxRQUFNRyxnQkFBZ0IsR0FBR0UsVUFBVSxDQUFDSCxXQUFYLEVBQXpCO0FBRUEsU0FBTTtBQUNGSixJQUFBQSxLQUFLLEVBQUM7QUFDRkssTUFBQUE7QUFERTtBQURKLEdBQU47QUFTSDtBQUVELGlFQUFlTixLQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vaWRlaWEtdW5pY2EvLi9wYWdlcy90ZW1wbzIuanM/MTU1OCJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBUZW1wbyhwcm9wcyl7XG4gICAgY29uc3QgZHluYW1pY0RhdGUgPSBuZXcgRGF0ZSgpXG4gICAgY29uc3QgZHluYW1pY0RhdGVTdHJpbmcgPSBkeW5hbWljRGF0ZS50b0dNVFN0cmluZygpXG4gICAgcmV0dXJuKFxuPD5cbiAgICAgICAgICAgIFxuICAgICAgICAgICAgPGRpdj57ZHluYW1pY0RhdGVTdHJpbmd9IChEaW7Dom1pY28pIC0gPC9kaXY+XG4gICAgICAgICAgICA8ZGl2Pntwcm9wcy5zdGF0aWNEYXRlU3RyaW5nfSAgKEVzdMOhdGljbylnZXRTdGF0aWNQcm9wcyAtIHJvZGEgdW1hIHZleiBubyBidWlsZCBkZSBwcm9kdcOnw6NvPC9kaXY+XG48Lz5cbiAgICApXG5cbiAgICB9XG5leHBvcnQgZnVuY3Rpb24gZ2V0U3RhdGljUHJvcHMoKXtcbiAgICBjb25zdCBzdGF0aWNEYXRlID0gbmV3IERhdGUoKVxuICAgIGNvbnN0IHN0YXRpY0RhdGVTdHJpbmcgPSBzdGF0aWNEYXRlLnRvR01UU3RyaW5nKClcblxuICAgIHJldHVybntcbiAgICAgICAgcHJvcHM6e1xuICAgICAgICAgICAgc3RhdGljRGF0ZVN0cmluZ1xuXG4gICAgICAgIH1cblxuXG4gICAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IFRlbXBvIl0sIm5hbWVzIjpbIlRlbXBvIiwicHJvcHMiLCJkeW5hbWljRGF0ZSIsIkRhdGUiLCJkeW5hbWljRGF0ZVN0cmluZyIsInRvR01UU3RyaW5nIiwic3RhdGljRGF0ZVN0cmluZyIsImdldFN0YXRpY1Byb3BzIiwic3RhdGljRGF0ZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/tempo2.js\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/tempo2.js"));
module.exports = __webpack_exports__;

})();